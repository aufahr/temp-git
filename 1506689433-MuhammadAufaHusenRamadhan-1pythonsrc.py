
# coding: utf-8

# # UTS1 PDB  : Stanford Car Dataset Classification
# 
# 
# ---
# 
# 
# **Nama : Muhammad Aufa Husen Ramadhan**  
# **NPM :1506689433**

# In[1]:


import os
HOME_PATH = os.getcwd()
DATA_PATH = HOME_PATH +"/data"
FILEMAP_PATH = HOME_PATH +"/file_map.csv"
EXPERIMENT_RESULT_PATH = HOME_PATH + "/result"
HOME_STATE_PATH = HOME_PATH + "/state"


# In[2]:


import os
import csv
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import math 
import cv2
import pickle
import h5py
from keras.preprocessing import image as kimg
import pickle
import keras
from pathlib import Path


# In[3]:


def save_state_to_pickle(filename, data, path_type='state'):
    save_path = HOME_STATE_PATH
    if 'result' ==  path_type:
        save_path = EXPERIMENT_RESULT_PATH
        
    with open("{}/{}".format(save_path, filename), 'wb+') as hf:
        pickle.dump(data,hf, protocol=pickle.HIGHEST_PROTOCOL)
      
def load_state_from_pickle(filename, path_type='state'):
    save_path = HOME_STATE_PATH
    if 'result' ==  path_type:
        save_path = EXPERIMENT_RESULT_PATH
        
    with open("{}/{}".format(save_path, filename), 'rb') as hf:
        return pickle.load(hf)

def file_exsist(path):
  return Path(path).is_file()

def file_exsist_in_state(name):
  return file_exsist("{}/{}".format(HOME_STATE_PATH, name))


# In[4]:


# GENERATE FILE MAPPING !

# get the training labels
train_labels = os.listdir(DATA_PATH)

# sort the training labels
train_labels.sort()

with open(FILEMAP_PATH, 'w+') as file :
    # Write HEADER for csv file 
    writer = csv.writer(file)
    writer.writerow([ "File_Path", "Label"])

    # loop over the training data sub-folders
    for training_name in train_labels:
        # join the training data path and each species training folder
        dir = os.path.join(DATA_PATH, training_name)
        # get the current training label
        current_label = training_name

        for file_name in  [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]:
          writer.writerow([os.path.join(dir, file_name),training_name])
    
filemap = pd.read_csv(FILEMAP_PATH)
filemap.describe()


# In[5]:


filemap = pd.read_csv(FILEMAP_PATH)

#  get min number of image per class 
labels = filemap["Label"].unique()
image_class_file_map = {}

max_class_count = 0
min_class_count = None
total_count = 0

for label in labels:
  label_image_paths = filemap.loc[filemap['Label'] == label]['File_Path']
  image_class_file_map[label] = label_image_paths

  total_count = total_count + len(label_image_paths)

  if(len(label_image_paths) > max_class_count):
    max_class_count = len(label_image_paths)

  if(min_class_count is None or len(label_image_paths) < min_class_count ):
    min_class_count = len(label_image_paths)

avg = total_count/len(labels)

print("Max data per class : {}".format(max_class_count))
print("Min data per class : {}".format(min_class_count))
print("Average data per class : {}".format(math.floor(avg)))
print("Class count : {}".format(len(labels)))


# In[6]:


from keras.utils import np_utils
re_run_on_already_exsist = True
if(not file_exsist_in_state('extracted_features_2.pickle') or re_run_on_already_exsist ):
  # setup raw experiment data (still based on path) 
  all_x = []
  all_y = []
  
  if(True):
    for img_class, images in image_class_file_map.items():
      for img in images :
        all_x.append(img)
        all_y.append(img_class)

    x_raw_features = []
    for idx, x in enumerate(all_x):
        # read the image and resize it to a fixed-size
        img = kimg.load_img(x, target_size=(224, 224))
        img_arr = kimg.img_to_array(img)
        x_raw_features.append(img_arr)
        if(idx == len(all_x) -1 or idx % 1000 == 0):
            print("{}/{}".format(idx, len(all_x)))
    
    class_map_dict = {}
    for i, y in enumerate(labels):
        class_map_dict[y] = i

    save_state_to_pickle('class_map_dict.pickle', class_map_dict)
    all_y = [class_map_dict[i] for i in all_y]
    
    raw_setup = {
        'x' : x_raw_features,
        'y' : np_utils.to_categorical(all_y, num_classes=len(labels))
    }
    
    save_state_to_pickle('extracted_features_2.pickle', raw_setup)
else:
    print("Loading data from pickle")
    raw_setup = load_state_from_pickle('extracted_features_2.pickle')
    print("Done loading data from pickle")


# In[7]:


max_number_of_data_per_class = 60
all_x = raw_setup['x']
all_y = raw_setup['y']

# get certain number of data 
class_img_count = {}
new_x = []
new_y = []

for i in range(0, len(all_x)):
    img_arr = all_x[i]
    label = hash(tuple(all_y[i]))
    label_val = all_y[i]
    
    if label not in class_img_count:
      class_img_count[label] = 0
    
    if(class_img_count[label] < max_number_of_data_per_class):
      new_x.append(img_arr)
      new_y.append(label_val)
      class_img_count[label] = class_img_count[label] + 1
    
print("Done getting images/class")


# In[8]:


from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input as vgg16_preprocess_input
from keras.applications import ResNet50
from keras.applications.resnet50 import preprocess_input as resnet50_preprocess_input
from keras.applications.vgg19 import VGG19
from keras.applications.vgg19 import preprocess_input as vgg19_preprocess_input
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.inception_resnet_v2 import preprocess_input as incresv2_preprocess_input

# default weights = imagenet
pretrained_models = {
    "InceptionResNetV2" : {'model' : InceptionResNetV2, 'preprocess' : incresv2_preprocess_input, 'input_shape' : (224, 224,3)},
    "VGG16": {'model' : VGG16, 'preprocess' : vgg16_preprocess_input, 'input_shape' : (224, 224,3), 'output_shape' : (7,7,512)},
    "VGG19": {'model' : VGG19, 'preprocess' : vgg19_preprocess_input, 'input_shape' : (224, 224,3), 'output_shape' : (7,7,512)},
    "ResNet50": {'model' : ResNet50, 'preprocess' : resnet50_preprocess_input, 'input_shape' : (224, 224),'output_shape' : (7,7,2048)},
}


# In[ ]:


from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from keras.callbacks import ReduceLROnPlateau
from keras import models
from keras import layers
from keras import optimizers
from livelossplot import PlotLossesKeras
from keras.optimizers import SGD
import gc
 

train_ratios_options = [0.8, 0.6, 0.5]
batch_size = 100
num_epochs = 100
patience = 5

def prepareModel(pt_model):
    # Create the model
    model = models.Sequential()

    # Add the vgg convolutional base model
    pt_mdl = pt_model['model'](weights='imagenet', include_top=False, input_shape=(224, 224, 3))
    
    for layer in pt_mdl.layers[:-6]:
        layer.trainable = False
    
    model.add(pt_mdl)

    # Add new layers
    model.add(layers.Flatten())
    model.add(layers.Dense(1024, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(196, activation='softmax'))

    # Show a summary of the model. Check the number of trainable parameters
    model.summary()
    
    return model

for train_ratio in train_ratios_options:
  
        print("Split to train test for train ratio {}".format(train_ratio))
#         pre shuffle (not sure)
        new_x = np.asarray(new_x)
        new_y = np.asarray(new_y)
        p = np.random.permutation(len(new_x))
        new_x = new_x[p]
        new_y = new_y[p]
        print(new_x.shape)
        print(new_y.shape)
    
        X_train, X_test, y_train, y_test = train_test_split(new_x, new_y, test_size=(1-train_ratio), random_state=32)
        print("Convert to np array")
        X_train =  np.asarray(X_train)
        X_test  =  np.asarray(X_test)
        y_train = np.asarray(y_train)
        y_test = np.asarray(y_test)
                            
        print(X_train.shape)
        print(y_train.shape)
        print(X_test.shape)
        print(y_test.shape)

        for key, pt_model in pretrained_models.items() :
            try:
                #  train here
                experimentKey = "{}{}".format(key, train_ratio)
                print("Start training for {} w/ train ratio of {}".format(key, train_ratio))
                print("Preprocess using model preprocessing method")
                fX_train = pt_model['preprocess'](X_train)
                fX_test = pt_model['preprocess'](X_test)
                print(fX_train.shape)
                print(fX_test.shape)
                print("Done preprocessing train and test data")

                print("Preparing model")
                model = prepareModel(pt_model)
                model.compile(loss='categorical_crossentropy',
                      optimizer= 'sgd',
                      metrics=['acc'])               


                print("Preparing image data generator")
                train_datagen = ImageDataGenerator(rotation_range=20.,
                                                   width_shift_range=0.1,
                                                   height_shift_range=0.1,
                                                   zoom_range=0.2,
                                                   horizontal_flip=True)
                test_datagen = ImageDataGenerator()

                model_training_log_dir = "result/{}".format(experimentKey)
                if not os.path.exists(model_training_log_dir):
                    os.mkdir(model_training_log_dir)     

                print("Preparing for training")

                # callbacks
                tensor_board = keras.callbacks.TensorBoard(log_dir=model_training_log_dir, 
                                                           histogram_freq=0,
                                                           write_graph=True,
                                                           write_images=True)

                early_stop = EarlyStopping('val_acc', patience=patience)
                reduce_lr = ReduceLROnPlateau('val_acc', factor=0.1, patience=int(patience/5), verbose=1)
                model_names = model_training_log_dir +'/' + experimentKey + '.{epoch:02d}-{val_acc:.2f}.hdf5'
                model_checkpoint = ModelCheckpoint(model_names, monitor='val_acc', verbose=1, save_best_only=True, period=10)
                csv_logger = CSVLogger('{}/{}'.format(model_training_log_dir, 'training.csv'), append=True, separator=';')

                callbacks = [tensor_board, model_checkpoint, reduce_lr, csv_logger]

                print("Start training")
                history = model.fit_generator(train_datagen.flow(fX_train, y_train, batch_size = batch_size),
                                              steps_per_epoch=fX_train.shape[0]/ batch_size,
                                              validation_data=test_datagen.flow(fX_test, y_test, batch_size = batch_size),
                                              validation_steps=fX_test.shape[0] / batch_size,
                                              epochs=num_epochs,
                                              callbacks=callbacks,
                                              verbose=1)  
                
                del history
                del model
                gc.collect()
            except Exception as e:
                print(e)
        

